# Clojure Njord Client

[![Clojars Project](https://img.shields.io/clojars/v/br.com.vikingmakt/njord.svg)](https://clojars.org/br.com.vikingmakt/njord)

Connect to Njord servers thru RabbitMQ

**This lib only supports the [langohr](https://github.com/michaelklishin/langohr) connector**

## HOW TO USE

### Connecting to RabbitMQ

```clojure
(require '[langohr.core :as rmq])

(def rmq-connection (rmq/connect {:uri "amqp://localhost:5672/%2f"}))
```

---

### Starting Elasticsearch service

```clojure
(require '[njord.core :as njord])

(def elastic-inst (njord/start-elastic rmq-connection))
```

### Requesting to Elasticsearch

```clojure
(require '[njord.elasticsearch :as elasticsearch])

(let [url "http://localhost:9200/test/type1/1/_update"
      method "POST"
      body {:script {:source "ctx._source.counter += params.count"
                     :lang "painless"
                     :params {:count 4}}}
      {:keys [status payload err]} (elasticsearch/request elastic-inst {:url url :method method :payload body})]
  (println status)
  (println payload))
```

```sh
>>> 200
>>> {
	:_shards {
		:total 0
		:successful 0
		:failed 0
	}
	:_index "test"
	:_type "type1"
	:_id "1"
	:_version 6
	:result "noop"
}
```

### Requesting to Elasticsearch Async

```clojure
(elasticsearch/request
  elastic-inst
  {:url url :method method :payload body}
  (fn [result]
    (let [{:keys [status payload err]} @result]
      (println status)
      (println payload))))
```

---

### Starting HTTP service

```clojure
(require '[njord.core :as njord])

(def http-inst (njord/start-http rmq-connection))
```

### Requesting to HTTP

```clojure
(require '[njord.http :as http])

(let [url "http://google.com"
      {:keys [code body headers request_time]} (http/request http-inst {:url url})]
  (println code)
  (println headers)
  (println request_time))
```

```sh
>>> 200
>>> {
	:X-Frame-Options "SAMEORIGIN"
	:X-XSS-Protection "1; mode=block"
	:Date "Thu, 01 Feb 2018 02:36:40 GMT"
	:Cache-Control "private, max-age=0"
	:Server "gws"
	:Content-Type "text/html; charset=ISO-8859-1"
	:Connection "close"
	:Expires "-1"
	:P3P "CP=\"This is not a P3P policy! See g.co/p3phelp for more info.\""
}
>>> 0.331
```

### Requesting to HTTP Async

```clojure
(http/request
  http-inst
  {:url url}
  (fn [result]
    (let [{:keys [code body headers request_time]} @result]
      (println code)
      (println headers)
      (println request_time))))
```

## CHANGELOG

|Version|Description                     |
|-------|--------------------------------|
|0.3.0  |Elasticsearch client            |
|0.2.1  |Replace async to augustus lib   |
|0.2.0  |SOAP client                     |
|0.1.0  |Thread pool and Kernel requester|
|0.0.1  |HTTP Client                     |
