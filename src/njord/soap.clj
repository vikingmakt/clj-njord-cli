(ns njord.soap
  (:require [augustus.async :as async]
            [augustus.future :as afuture]
            [cheshire.core :as json]
            [clojure.data.codec.base64 :as b64]
            [langohr.channel :as channel]
            [langohr.queue :as queue]
            [njord.client :refer [map->NjordClient]]
            [njord.common :refer [uuid4]]
            [njord.kernel :as kernel])
  (:import com.novemberain.langohr.Connection
           njord.client.NjordClient)
  (:gen-class))

(def ^:const exchange-headers "njord_soap_headers")
(def ^:const exchange-topic "njord_soap")
(def ^:const rk "njord.soap.execute")

(defn- decode [msg]
  (json/parse-string (new String ^bytes (b64/decode (.getBytes ^String msg))) true))

(defn- encode [msg]
  (b64/encode (.getBytes ^String (json/generate-string msg))))

(defn- queue-bind [io]
  (->> {:arguments {"requester" (:uid io)
                    "njord-soap-response" true
                    "x-match" "all"}}
       (queue/bind (:ch io) kernel/queue-name exchange-headers)))

(defn execute-async [io request & [cb]]
  {:pre [(instance? NjordClient io) (= (:type io) :soap) (or (nil? cb) (fn? cb))]}
  (let [body (encode request)
        headers {"service" "execute"}
        fut (kernel/request io exchange-topic rk body headers decode)]
    (if cb
      (afuture/add-future-callback fut cb))
    fut))

(defn execute [io request]
  (deref (execute-async io request)))

(defn start [conn]
  {:pre [(instance? Connection conn)]}
  (let [io (map->NjordClient
            {:ch (channel/open conn)
             :uid (uuid4)
             :type :soap})]
    (kernel/start conn)
    (async/add-callback queue-bind io)
    io))
