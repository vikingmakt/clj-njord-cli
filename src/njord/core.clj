(ns njord.core
  (:require [njord.elasticsearch :as elastic]
            [njord.http :as http]
            [njord.soap :as soap])
  (:gen-class))

(defn start-elastic [conn]
  (elastic/start conn))

(defn start-http [conn]
  (http/start conn))

(defn start-soap [conn]
  (soap/start conn))
