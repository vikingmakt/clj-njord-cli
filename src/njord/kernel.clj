(ns njord.kernel
  (:require [augustus.future :as afuture]
            [langohr.basic :as basic]
            [langohr.channel :as channel]
            [langohr.queue :as queue]
            [langohr.consumers :as consumers]
            [njord.common :refer [sha256 uuid4]])
  (:gen-class))

(def queue-name (format "njord-kernel-%s" (uuid4)))

(def ^:private futures (atom {}))

(def ^:private running (atom false))

(defn- on-msg [chan {:keys [headers delivery-tag]} body]
  (let [etag (str (get headers "etag"))]
    (when (contains? @futures etag)
      ((get @futures etag) (new String ^bytes body "UTF-8"))
      (swap! futures dissoc etag)))
  (basic/ack chan delivery-tag))

(defn gen-etag [req]
  (sha256 (format "njord-client[%s:%s]" req (uuid4))))

(defn- queue-declare [conn]
  (doto (channel/open conn)
    (queue/declare queue-name {:durable false :exclusive true})
    (consumers/subscribe queue-name on-msg)))

(defn add-promise [etag prom]
  (swap! futures assoc etag prom))

(defn request [io exchange rk body headers decoder]
  (let [etag (gen-etag (:uid io))
        fut (afuture/new-future)]
    (add-promise etag #(fut (decoder %)))
    (basic/publish (:ch io) exchange rk body {:headers (assoc headers
                                                              "etag" etag
                                                              "requester" (:uid io))})
    fut))

(defn start [conn]
  (when-not @running
    (reset! running true)
    (queue-declare conn)))
