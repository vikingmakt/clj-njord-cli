(ns njord.common
  (:import java.security.MessageDigest
           java.util.UUID
           javax.xml.bind.DatatypeConverter)
  (:require [clojure.string :refer [lower-case]])
  (:gen-class))

(defn sha-digest [alg data]
  (-> (MessageDigest/getInstance alg) (.digest (.getBytes ^String data)) DatatypeConverter/printHexBinary lower-case))

(def sha1 #(sha-digest "sha1" %))

(def sha256 #(sha-digest "SHA-256" %))

(defn uuid4 []
  (str (UUID/randomUUID)))
