(ns njord.http
  (:require [augustus.async :as async]
            [augustus.future :as afuture]
            [langohr.channel :as channel]
            [langohr.queue :as queue]
            [njord.client :refer [map->NjordClient]]
            [njord.common :refer [uuid4]]
            [njord.kernel :as kernel]
            [utcode.core :as utcode])
  (:import com.novemberain.langohr.Connection
           njord.client.NjordClient)
  (:gen-class))

(def ^:const exchange-headers "njord_http_headers")
(def ^:const exchange-topic "njord_http")
(def ^:const rk "njord.http.request")

(defn- queue-bind [io]
  (->> {:arguments {"requester" (:uid io)
                    "njord-http-response" true
                    "x-match" "all"}}
       (queue/bind (:ch io) kernel/queue-name exchange-headers)))

(defn request-async [io request & [cb]]
  {:pre [(instance? NjordClient io) (= (:type io) :http) (or (nil? cb) (fn? cb))]}
  (let [body (utcode/encode request)
        headers {"service" "fetch"}
        fut (kernel/request io exchange-topic rk body headers utcode/decode)]
    (if cb
      (afuture/add-future-callback fut cb))
    fut))

(defn request [io request]
  (deref (request-async io request)))

(defn start [conn]
  {:pre [(instance? Connection conn)]}
  (let [io (map->NjordClient
            {:ch (channel/open conn)
             :uid (uuid4)
             :type :http})]
    (kernel/start conn)
    (async/add-callback queue-bind io)
    io))
