(defproject br.com.vikingmakt/njord "0.3.2"
  :description "Njord Client Connector"
  :url "https://gitlab.com/vikingmakt/clj-njord-cli"
  :license {:name "BSD-2-Clause"}
  :dependencies [[br.com.vikingmakt/augustus "0.1.12"]
                 [cheshire "5.8.0"]
                 [com.novemberain/langohr "3.7.0"]
                 [org.clojure/clojure "1.8.0"]
                 [org.clojure/data.codec "0.1.1"]
                 [utcode "0.2.3"]])
